package com.mab.jpa.recipe.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mab.jpa.recipe.model.Ingredient;
import com.mab.jpa.recipe.model.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConvertObject {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void fromObject2Json() throws JsonProcessingException {
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();

        String jsonIngredient = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(ingredient);
        System.out.println(jsonIngredient);

        String jsonRecipe = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(recipe);
        System.out.println(jsonRecipe);
    }
}
