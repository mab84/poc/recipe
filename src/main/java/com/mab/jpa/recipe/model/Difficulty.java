package com.mab.jpa.recipe.model;

public enum Difficulty {

    EASY, MODERATE, HARD
}
