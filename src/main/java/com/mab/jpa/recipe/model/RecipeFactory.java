package com.mab.jpa.recipe.model;

import java.util.HashSet;

//TODO: Create RecipeDto as builder, add mapstruct to save a Recipe object
// or find out the best way to do it, maybe it's right to have the builder in the entity
public class RecipeFactory {

    private RecipeFactory() {
    }

    public static Recipe getNewRecipe(String description, Integer prepTime, Integer cookTime, Integer servings,
                                      String source, String url, String directions, Difficulty difficulty) {

        return Recipe.builder()
                .description(description)
                .prepTime(prepTime)
                .cookTime(cookTime)
                .servings(servings)
                .source(source)
                .url(url)
                .directions(directions)
                .difficulty(difficulty)
                .image(null)
                .ingredients(new HashSet<>()) //has to be initialized otherwise, NullPointerException
                .categories(new HashSet<>()) //has to be initialized otherwise, NullPointerException
                .build();
    }
}
