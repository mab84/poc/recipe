package com.mab.jpa.recipe.service;

import com.mab.jpa.recipe.model.*;
import com.mab.jpa.recipe.repository.CategoryRepository;
import com.mab.jpa.recipe.repository.RecipeRepository;
import com.mab.jpa.recipe.repository.UnitOfMeasureRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.mab.jpa.recipe.model.Difficulty.MODERATE;
import static com.mab.jpa.recipe.model.RecipeFactory.getNewRecipe;
import static java.math.BigDecimal.valueOf;

@Slf4j
@Component
@AllArgsConstructor
public class IndexService implements ApplicationListener<ContextRefreshedEvent> {

    private final RecipeRepository recipeRepository;
    private final CategoryRepository categoryRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Recipe guacamoleRecipe = createGuacamoleRecipe();
        log.info("Recipe successfully created: {}", guacamoleRecipe);
    }

    public Category getCategoryByDescription(String description) {
        return categoryRepository.findByDescription(description).orElseThrow(() -> new RuntimeException("Category not found"));

    }

    public UnitOfMeasure getUnitOfMeasureByDescription(String description) {
        return unitOfMeasureRepository.getByDescription(description).orElseThrow(() -> new RuntimeException("Unit of measure not found"));
    }

    public Recipe createGuacamoleRecipe() {
        String directions = "Cut the avocados in half. Remove the pit. Score the inside of the avocado with a blunt " +
                "knife and scoop out the flesh with a spoon. (See How to Cut and Peel an Avocado.) Place in a bowl.Using a fork, " +
                "roughly mash the avocado. (Don't overdo it! The guacamole should be a little chunky.) " +
                "Sprinkle with salt and lime (or lemon) juice. The acid in the lime juice will provide some balance to the richness " +
                "of the avocado and will help delay the avocados from turning brown.Add the chopped onion, cilantro, " +
                "black pepper, and chilis. Chili peppers vary individually in their spiciness. " +
                "So, start with a half of one chili pepper and add more to the guacamole to your desired degree of heat. " +
                "Remember that much of this is done to taste because of the variability in the fresh ingredients. " +
                "Start with this recipe and adjust to your taste. If making a few hours ahead, place plastic wrap on the surface " +
                "of the guacamole and press down to cover it to prevent air reaching it. (The oxygen in the air causes oxidation " +
                "which will turn the guacamole brown.) Garnish with slices of red radish or jigama strips. Serve with your choice of" +
                " store-bought tortilla chips or make your own homemade tortilla chips. Refrigerate leftover guacamole up to 3 days. " +
                "Note: Chilling tomatoes hurts their flavor. So, if you want to add chopped tomato to your guacamole, add it just before serving.";

        Recipe guacamole = getNewRecipe("How to Make Perfect Guacamole", 10, 1, 5,
                "simplyrecipes.com", "https://www.simplyrecipes.com/recipes/perfect_guacamole", directions, MODERATE);

        //Notes
        String notes = "\"I have been using this guac recipe for years and it is always a hit. People say it's the best " +
                "guac they ever had! I like to chop up my onion & serrano chilies in my mini food processor though - " +
                "makes quick work of it! <3\"";

        guacamole.setNotes(new Notes(notes));

        //Measures
        UnitOfMeasure tablespoon = getUnitOfMeasureByDescription("Tablespoon");
        UnitOfMeasure teaspoon = getUnitOfMeasureByDescription("Teaspoon");
        UnitOfMeasure pinch = getUnitOfMeasureByDescription("Pinch");
        UnitOfMeasure unit = getUnitOfMeasureByDescription("Unit");

        //Ingredients
        guacamole.getIngredients().add(new Ingredient("Cilantro finely chopped", valueOf(2L), tablespoon));
        guacamole.getIngredients().add(new Ingredient("Salt", valueOf(1.5), teaspoon));
        guacamole.getIngredients().add(new Ingredient("Fresh lime or lemon juice", valueOf(1L), tablespoon));
        guacamole.getIngredients().add(new Ingredient("Freshly ground black pepper", valueOf(1L), pinch));
        guacamole.getIngredients().add(new Ingredient("Ripe tomato, chopped", valueOf(1.5), unit));

        //Categories
        Category mexican = getCategoryByDescription("Mexican");
        Category american = getCategoryByDescription("American");

        guacamole.getCategories().add(mexican);
        guacamole.getCategories().add(american);

        return recipeRepository.save(guacamole);
    }

    public List<Recipe> getAllRecipes() {
        return (List<Recipe>) recipeRepository.findAll();
    }
}
